﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClonerTitel : MonoBehaviour
{
    public GameObject sheep;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        GameObject spawned = Instantiate(sheep, transform.position, Quaternion.identity);
        spawned.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-100, 100), Random.Range(-100, 2000)));
    }
}
