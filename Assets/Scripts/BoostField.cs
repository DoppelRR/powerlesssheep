﻿using UnityEngine;

public class BoostField : MonoBehaviour {
    public bool boostActive = true; //durch UI auf true setzen
    public float boostX;
    public float boostY;

    private void OnTriggerStay2D(Collider2D collision) {
        Rigidbody2D sheep = collision.attachedRigidbody;
        sheep.AddForce(new Vector2(boostX, boostY), ForceMode2D.Force);
    }

    void OnMouseOver() {
        if (Input.GetMouseButtonDown(1)) {
            Destroy(gameObject);
        }    
    }
}
