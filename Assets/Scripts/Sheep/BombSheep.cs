﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSheep : MonoBehaviour, ISheep
{

    public long baseDamage;
    public float radius = 5.0F;
    public float power = 10.0F;
    public float timer = 10.0F;
    public GameObject explosion;

    Rigidbody2D rb;
    public long getDamage() {
        Explode(false);
        return baseDamage;
    }

    void Explode(bool self) {
        Vector3 explosionPos = transform.position;
        Instantiate(explosion, explosionPos, Quaternion.identity);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(explosionPos, radius);
        foreach (Collider2D hit in colliders) {
            Rigidbody2D hitrb = hit.GetComponent<Rigidbody2D>();

            if (hitrb != null && hitrb != rb) { 
                Vector2 relPos = hitrb.position - rb.position;
                hitrb.AddForce(relPos.normalized*power/Mathf.Pow(relPos.magnitude,2), ForceMode2D.Impulse);
            }
        }
        if (self) {
            Destroy(gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void FixedUpdate() {
        timer -= Time.fixedDeltaTime;
        if (timer <= 0) {
            Explode(true);
        }
    }
}
