﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorSheep : MonoBehaviour, ISheep
{
    public long baseDamage;
    Rigidbody2D rb;

    public long getDamage() {
        return baseDamage;
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        rb.angularVelocity = -300;
    }
}
