﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloner : MonoBehaviour
{
    Gamemanager gamemanager;
    float lastSpawntime;

    // Start is called before the first frame update
    void Start() {
        lastSpawntime = Time.time;
        gamemanager = Gamemanager.gamemanager;
    }

    void FixedUpdate() {
        if(Time.time >= lastSpawntime + gamemanager.currentSecondsPerSheep) {
            GameObject sheep = GetSheep();
            if (!gamemanager.unlockedSheeps.Contains(sheep)) {
                gamemanager.UnlockSheep(sheep);
            }
            GameObject newSheep = Instantiate(sheep,transform.position, Quaternion.identity);
            newSheep.GetComponent<Rigidbody2D>().AddTorque(Random.Range(-2,2));
            lastSpawntime = Time.time;
        }
    }

    GameObject GetSheep() {
        int valueUntilHere = 0;
        int randval = (int)Random.Range(0, 100);
        foreach(GameObject sheep in gamemanager.sheepChance.Keys) {
            valueUntilHere += gamemanager.sheepChance[sheep];
            if (randval<valueUntilHere) {
                if(sheep == gamemanager.basicSheep) {
                    int mutate = (int)Random.Range(0, 100);
                    if (mutate < gamemanager.mutantSheeps.Count) {
                        GameObject mutsheep = gamemanager.mutantSheeps[mutate];
                        return mutsheep;
                    }
                } 
                return sheep;
            }
        }
        return null;
    }
}
