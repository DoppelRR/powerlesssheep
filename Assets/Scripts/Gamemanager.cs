﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gamemanager : MonoBehaviour {
    
    public static Gamemanager gamemanager = null;
    private Camera cam;
    public GameObject cloner;
    public GameObject basicSheep;
    public GameObject coin;
    public List<GameObject> mutantSheeps;
    public List<GameObject> unlockedSheeps;
    public Dictionary<GameObject, int> sheepChance;
    public GameObject boostField;
    public GameObject sheepTypeUpgrade;
    public Button nextLevelButton;

    public float secondsPerSheep;
    public float currentSecondsPerSheep;
    public long money;
    public long dnaPoints;
    public long totalDnaPoints;
    public long startGateHealth;
    public long gateHealth;
    public bool nextLevel = false;
    public float rewardMoney;
    public float sheepDamageMultiplier = 1;
    public long rewardDna;
    public int coinChance;
    public float clonerX;
    public float clonerY;

    public int levelClonerCount;
    public long priceClonerCount;

    public long priceBoostField;
    private bool boostfieldPurchased;

    public int levelClonerSpeed;
    public long priceClonerSpeed;

    public void UpgradeClonerCount() {
        if (levelClonerCount < 4) {
            money -= priceClonerCount;
            priceClonerCount = (long) (priceClonerCount * 3f);
            Instantiate(cloner, new Vector3(clonerX + (levelClonerCount) * 1.5f, clonerY, 0), Quaternion.identity);
            levelClonerCount++;
        }
    }

    public void UpgradeClonerSpeed() {
        money -= priceClonerSpeed;
        priceClonerSpeed = (long) (priceClonerSpeed * 1.2f);
        currentSecondsPerSheep *= 0.95f;
        levelClonerSpeed++;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        GameObject gameObjectButton = GameObject.FindGameObjectWithTag("Button");
        nextLevelButton = gameObjectButton.transform.GetComponent<Button>();
        nextLevelButton.onClick.AddListener(NextLevel);
        gameObjectButton.SetActive(false);
        currentSecondsPerSheep = secondsPerSheep;
        money = 100;
        levelClonerCount = 1;
        priceClonerCount = 50;
        levelClonerSpeed = 0;
        priceClonerSpeed = 10;
        boostfieldPurchased = false;
        priceBoostField = 10;
        gateHealth = startGateHealth;
        cam = Camera.main;
        sheepChance = new Dictionary<GameObject, int>();
        unlockedSheeps = new List<GameObject>();
        UnlockSheep(basicSheep);
        for (int i = 0; i < levelClonerCount; i++) {
            Instantiate(cloner, new Vector3(clonerX+i*1.5f, clonerY, 0), Quaternion.identity);
        }
    }

    public void NextLevel() {
        nextLevelButton.gameObject.SetActive(false);
        startGateHealth *= 2;
        rewardMoney *= 1.2f;
        sheepDamageMultiplier *= 1.2f;
        totalDnaPoints++;
        dnaPoints= totalDnaPoints;
        
        SceneManager.LoadScene("Game");
    }
    public void UnlockSheep(GameObject sheep) {
        if(sheep!=basicSheep && !mutantSheeps.Contains(sheep) || sheep==null || unlockedSheeps.Contains(sheep)) {
            return;
        }

        unlockedSheeps.Add(sheep);
        if (sheep == basicSheep) {
            sheepChance.Add(basicSheep, 100);
        } else {
            sheepChance.Add(sheep, 1);
            sheepChance[basicSheep]--;
        }
        GameObject newUpgrade = Instantiate(sheepTypeUpgrade, GameObject.FindGameObjectWithTag("UpgradeContentHolder").transform);
        RectTransform rt = newUpgrade.GetComponent<RectTransform>();
        rt.GetComponent<ChanceManager>().sheep = sheep;
        rt.anchoredPosition = new Vector2(300+120*unlockedSheeps.Count,0);
        rt.Find("Title").GetComponent<Text>().text = sheep.GetComponent<Description>().sheepName;
        rt.Find("Description").GetComponent<Text>().text = sheep.GetComponent<Description>().description;
        if (sheep == basicSheep) {
            rt.Find("ButtonUp").gameObject.SetActive(false);
            rt.Find("ButtonDown").gameObject.SetActive(false);
        }
        ((RectTransform)GameObject.FindGameObjectWithTag("UpgradeContentHolder").transform).sizeDelta = new Vector2(360 + 120 * unlockedSheeps.Count, 140);
    }

    public void PlaceBoostfield() {
        while (Input.GetMouseButtonDown(0)) {};
        boostfieldPurchased = true;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            SceneManager.LoadScene("MainMenu");
            Destroy(gameObject);
        }

        if (Input.GetKeyDown(KeyCode.L)) {
            currentSecondsPerSheep = 0;
        }

        if (Random.Range(0,10000) < coinChance) {
            Instantiate(coin, new Vector3(Random.Range(0, 17), Random.Range(0, 10), 4), Quaternion.identity);
        }

        if (boostfieldPurchased && Input.GetMouseButtonDown(0)) {
            money -= priceBoostField;
            Vector3 pos = cam.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            Instantiate(boostField,pos , Quaternion.identity);
            boostfieldPurchased = false;
        }
    }

    void Awake() {
        if (gamemanager == null) {
            gamemanager = this;
            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        else {
            Destroy(gameObject);
        }
    }

    public bool DamageGate(long damage) {
        money += (long) (damage * rewardMoney);
        damage = (long)(damage * sheepDamageMultiplier);
        gateHealth -= damage;
        return gateHealth <= 0;
    }
}
