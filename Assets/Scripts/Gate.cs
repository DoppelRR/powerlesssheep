﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public GameObject dmgNumber;
    AudioSource audioSource;
    TextMesh hpBar;
    Gamemanager gamemanager;
    // Start is called before the first frame update
    void Start()
    {
        gamemanager = Gamemanager.gamemanager;
        GameObject healthbar = Instantiate(dmgNumber, transform.position + new Vector3(0, 6, 0), Quaternion.identity);
        TimeToLive tml = healthbar.GetComponent<TimeToLive>();
        audioSource = GetComponent<AudioSource>();
        tml.active = false;

        hpBar = healthbar.GetComponent<TextMesh>();

    }

    void Update() {
        hpBar.text = gamemanager.gateHealth.ToString();
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        long damage = collision.collider.gameObject.GetComponent<ISheep>().getDamage();
        GameObject number = Instantiate(dmgNumber, collision.collider.transform.position+new Vector3(-1,0,0), Quaternion.identity);
        number.GetComponent<TimeToLive>().time = 2;
        number.GetComponent<TextMesh>().text = ((long) (damage * gamemanager.sheepDamageMultiplier)).ToString();
        Damage(damage);
        Destroy(collision.collider.gameObject);
    }

    private void Damage(long damage) {
        if (gamemanager.DamageGate(damage)) {
            gamemanager.GetComponent<AudioSource>().Play();
            gamemanager.nextLevelButton.gameObject.SetActive(true);
            Destroy(hpBar);
            Destroy(gameObject);
        }
        else {
            audioSource.Play();
        }
    }
}
