﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class MoneyText : MonoBehaviour
{
    Gamemanager gamemanager;
    Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        gamemanager = Gamemanager.gamemanager;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = gamemanager.money.ToString();
    }
}
