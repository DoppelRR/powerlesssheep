﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChanceManager : MonoBehaviour
{
    Gamemanager gamemanager;
    Button up;
    Button down;
    Text value;
    public GameObject sheep;

    void Increase() {
        if (gamemanager.sheepChance[gamemanager.basicSheep] > 0 && gamemanager.dnaPoints>=1) {
            gamemanager.dnaPoints--;
            gamemanager.sheepChance[sheep]++;
            gamemanager.sheepChance[gamemanager.basicSheep]--;
        }
    }

    void Decrease() {
        if (gamemanager.sheepChance[sheep] > 1) {
            gamemanager.dnaPoints++;
            gamemanager.sheepChance[sheep]--;
            gamemanager.sheepChance[gamemanager.basicSheep]++;
        }
    }

    void Start() {
        gamemanager = Gamemanager.gamemanager;
        up = transform.Find("ButtonUp").GetComponent<Button>();
        down = transform.Find("ButtonDown").GetComponent<Button>();
        value = transform.Find("Value").GetComponent<Text>();
        up.onClick.AddListener(Increase);
        down.onClick.AddListener(Decrease);
    }

    void Update() {
        value.text = gamemanager.sheepChance[sheep].ToString() + "%";
    }
}
