﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetBoosField : MonoBehaviour
{
    private Button button;
    private Text buttonText;
    private Text valueText;
    private AudioSource audioSource;
    Gamemanager gamemanager;

    // Start is called before the first frame update
    void Start() {
        gamemanager = Gamemanager.gamemanager;
        button = transform.Find("Button").GetComponent<Button>();
        buttonText = button.transform.Find("Text").GetComponent<Text>();
        valueText = transform.Find("Value").GetComponent<Text>();
        audioSource = GetComponent<AudioSource>();
        valueText.text = "Buy to place a Boost Field. Rightclick it to Remove.";
        button.enabled = false;
        button.onClick.AddListener(gamemanager.PlaceBoostfield);
        button.onClick.AddListener(playSound);
    }

    void playSound() {
        audioSource.Play();
    }

// Update is called once per frame
void Update() {

        buttonText.text = gamemanager.priceBoostField.ToString() + "$";
        if (gamemanager.money >= gamemanager.priceBoostField) {
            button.enabled = true;
        }
        else {
            button.enabled = false;
        }
    }
}
