﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeClonerSpeed : MonoBehaviour
{
    private Button button;
    private Text buttonText;
    private Text valueText;
    private AudioSource audioSource;
    Gamemanager gamemanager;

    // Start is called before the first frame update
    void Start() {
        gamemanager = Gamemanager.gamemanager;
        button = transform.Find("Button").GetComponent<Button>();
        buttonText = transform.Find("Button").Find("Text").GetComponent<Text>();
        valueText = transform.Find("Value").GetComponent<Text>();
        audioSource = GetComponent<AudioSource>();
        button.enabled = false;
        button.onClick.AddListener(gamemanager.UpgradeClonerSpeed);
        button.onClick.AddListener(playSound);
    }

    void playSound() {
        audioSource.Play();
    }

    // Update is called once per frame
    void Update() {
        valueText.text = gamemanager.levelClonerSpeed.ToString();

        buttonText.text = gamemanager.priceClonerSpeed.ToString() + "$ for the next Upgrade";
        if (gamemanager.money >= gamemanager.priceClonerSpeed) {
            button.enabled = true;
        }
        else {
            button.enabled = false;
        }
    }
}
