﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToLive : MonoBehaviour
{
    public float time;
    public bool active;
    private void FixedUpdate() {
        if (active) {
            if (time <= 0) {
                Destroy(gameObject);
            }
            time -= Time.fixedDeltaTime;
        }
    }
}
